# User Manager (Laravel API)

This is the back end API of user manager system built with [Laravel](https://laravel.com/).

## About

This api enables the front end interface to authenticate using [JWT](https://jwt.io/) authentication.It also repsonds to authorized requests from front end interface to facilitate CRUD operations on Users.

### Prerequisites

You need to have laravel installed on your system/server.

```
composer global require laravel/installer
```

### Installing

Follow these steps to set up the api.

Navigate to the project folder and run following command.

```
composer install
```
After this you need to go to the .env file and add your database credentials there.After that,
 run following to create tables.

```
php artisan migrate
```
Then to seed the admin user , run following command. 



```
php artisan db:seed --class=UsersTableSeeder
```

The admin credentials created for the front end interface thus created are.

```
User name: admin@demo.com
Password : Password123
```

Optionally,if you want to add some test uses in the users list,run following command.

```
php artisan db:seed --class=AppUsersTableSeeder
```

## Deployment

To run on a local server run following command.

```
php artisan serve
```

## Built With

* [Laravel](https://laravel.com/) - The web framework used
* [JWT](https://jwt.io/) - Authentication



## Authors

* **Shahzan M Siddique** - 


