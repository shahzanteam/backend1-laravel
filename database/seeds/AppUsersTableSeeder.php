<?php

use Illuminate\Database\Seeder;

class AppUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('app_users')->insert([
            'first_name' => 'user',
            'last_name' => '1',
            'email' => 'user1@demo.com',
            'phone' => '9876543210'
        ]);
        DB::table('app_users')->insert([
            'first_name' => 'user',
            'last_name' => '2',
            'email' => 'user2@demo.com',
            'phone' => '9876543210'
        ]);
        DB::table('app_users')->insert([
            'first_name' => 'user',
            'last_name' => '3',
            'email' => 'user3@demo.com',
            'phone' => '9876543210'
        ]);
    }
}
