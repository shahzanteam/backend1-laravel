<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AppUsers;

class UsersController extends Controller
{
  /*Function to get all users*/
  public function getAllUsers()
  {
  	return AppUsers::get();
  } 

  /*Function delete a user*/
  public function deleteUser(Request $request)
  {
  	$result=AppUsers::where('id',$request->id)->delete();
  	return array("success" => true ,"message"=>"User deleted.");
  }  

  /*Function add/edit a user*/
  public function addUser( Request $request)
  {  
  	switch ($request->mode) {
  	case 'new':
  		{
	 $check1= AppUsers::firstOrNew(['email' => $request->email]);
  	 $check2= AppUsers::firstOrNew(['phone' => $request->phone]);
     if($check1->exists){
       return array("success" => false ,"message"=>"User exists with same email.");
       }
       else if($check2->exists){
       	return array("success" => false ,"message"=>"User exists with same phone.");
       }
       else{
       	$result=AppUsers::create([
       		'first_name'=>$request->first_name,
       		'last_name'=>$request->last_name,
       		'email'=>$request->email,
       		'phone'=>$request->phone,
       	]);
       	return  array("success" => true ,"message"=>"User added");
       }
  		}
  		break;
  	
  	   case 'edit':
  		{
  				$result=AppUsers::where('id',$request->id)->update([
       		'first_name'=>$request->first_name,
       		'last_name'=>$request->last_name,
       		'email'=>$request->email,
       		'phone'=>$request->phone,
       	]);
       	return  array("success" => true ,"message"=>"User updated");
  		}
  		break;
    }
  	 
  }
}
