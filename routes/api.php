<?php

Route::group([

    'middleware' => ['api'],
    

], function ($router) {

    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
    Route::get('getAllUsers','UsersController@getAllUsers');
    Route::post('addUser','UsersController@addUser');
    Route::post('deleteUser','UsersController@deleteUser');

});
